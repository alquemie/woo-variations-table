jQuery(document).ready(function($) {

  var ajaxurl       = localvars.ajax_url;
  var carturl       = localvars.cart_url;
  var gclicked      = 0;
  var vartable_ajax = localvars.vartable_ajax;
  var $fragment_refresh = '';
  
  console.log(vartable_ajax);
  
  /** Cart Handling */
  $supports_html5_storage = ( 'sessionStorage' in window && window['sessionStorage'] !== null );
  if (vartable_ajax == 1) {
    $fragment_refresh = {
        url: ajaxurl,
        // url: woocommerce_params.ajax_url,
        type: 'POST',
        data: { action: 'woocommerce_get_refreshed_fragments' },
        success: function( data ) {
            if ( data && data.fragments ) {

                $.each( data.fragments, function( key, value ) {
                    $(key).replaceWith(value);
                });

                if ( $supports_html5_storage ) {
                    sessionStorage.setItem( "wc_fragments", JSON.stringify( data.fragments ) );
                    sessionStorage.setItem( "wc_cart_hash", data.cart_hash );
                }
                console.log('refresh');
                $('body').trigger( 'wc_fragments_refreshed' );
            }
        }
    };
  }

  
  if (jQuery("table.vartable").length > 0) {
    jQuery("table.vartable").each(function(index) {
      
      var vartable            = jQuery(this);
      var random_id           = jQuery(this).data('random');
      var vartable_ajax       = jQuery(this).data('vartable_ajax');
      var cartredirect        = jQuery(this).data('cartredirect');
      var sorting             = jQuery(this).data('sort');
      var vartable_globalcart = jQuery(this).data('globalcart');
      var preorder            = jQuery(this).data('preorder');
      var preorder_direction  = jQuery(this).data('preorder_direction');
      
      // console.log(random_id);
      
      
      update_global_sum(jQuery(this).find('input[name="var_quantity"]'));
      
      // gift wrap
      if (vartable.find("input.var_gift_wrap").length > 0) {
        vartable.find("input.var_gift_wrap").on("change", function() {
          if(jQuery(this).is(":checked")) {
            jQuery(this).closest('tr').find(".cartcol input.gift_wrap").val("yes");
          } else {
            jQuery(this).closest('tr').find(".cartcol input.gift_wrap").attr("value", "");
          }
        });
      }
      
      
      if (vartable.find("div.qtywrap").length > 0) {
        
        
        
        
        jQuery("#tb_"+ random_id +" div.qtywrap").each(function() {
          var qtythis = jQuery(this);
          qtythis.find(".minusqty").on("click", function() {
            var valnum = parseInt(qtythis.find("input").val());
            var valmin = qtythis.find("input").attr('min');
            
            if( typeof valmin === 'undefined' || valmin === null ){
              valmin = 0;
            }
            
            if (qtythis.find("input").attr("step") && qtythis.find("input").attr("step") > 0) {
              var step = parseInt(qtythis.find("input").attr("step"));
            } else {
              var step = 1;
            }
            
            if (valnum - step >= valmin) {
              qtythis.find("input").val(valnum - step);
              qtythis.closest('tr').find(".cartcol input.hidden_quantity").val(valnum - step);
              qtythis.find("input").trigger( "qty:change" );
            }
          });
          qtythis.find(".plusqty").on("click", function() {
            var valnum = parseInt(qtythis.find("input").val());
            var valmax = qtythis.find("input").attr('max');
            
            if( typeof valmax === 'undefined' || valmax === null ){
              valmax = -1;
            }
            
            if (qtythis.find("input").attr("step") && qtythis.find("input").attr("step") > 0) {
              var step = parseInt(qtythis.find("input").attr("step"));
            } else {
              var step = 1;
            }
            if ((valnum + step <= valmax) || valmax == -1) {
              qtythis.find("input").val(valnum + step);
              qtythis.closest('tr').find(".cartcol input.hidden_quantity").val(valnum + step);
              qtythis.find("input").trigger( "qty:change" );
            }
          });
        });
      }
      
      
      if (sorting == 'yes') {

        var $table = jQuery("#tb_"+ random_id +"").stupidtable();
        if (preorder != '' && preorder != 'custom') {
          var $th_to_sort = $table.find("thead th."+preorder);
          $th_to_sort.stupidsort();
        }
        if ($th_to_sort !== undefined && $th_to_sort !== null) {
          if (preorder_direction != '' && preorder_direction != 'custom') {
            $th_to_sort.stupidsort(preorder_direction);
          } else {
            $th_to_sort.stupidsort('asc');
          }
        }
        
        $table.bind('aftertablesort', function (event, data) {
            // data.column - the index of the column sorted after a click
            // data.direction - the sorting direction (either asc or desc)
            // $(this) - this table object

            // console.log("The sorting direction: " + data.direction);
            // console.log("The column index: " + data.column);
        });
        
        
      }

      
      
      
      jQuery("a#gc_"+ random_id +"_top, a#gc_"+ random_id +"_bottom").on("click", function(event) {
        gclicked = 1;
      });
  
      jQuery(".wishcol a").on("click", function(event) {
      
        event.preventDefault();
        
        var wthis = jQuery(this);
        jQuery.ajax({
          type:"GET",
          url: wthis.attr("href"),
          success:function(data){
            wthis.css({"font-size":"125%", "opacity": "0.6"});
                        
            if ((vartable_ajax != 1 && (vartable_globalcart == 1 || vartable_globalcart == 2)) || cartredirect == 'yes') {
              if (cartredirect == 'yes') {
                window.location.href = ""+carturl+"";
              }
              if (cartredirect == 'no') {
                location.reload();
              }
            }
            
            
          }
        });
      });
      
    });
    
    jQuery(document).on("submit", "form.vtajaxform", function(event) {

      event.preventDefault();
    
      console.log("triggered");
      
      if (jQuery(this).find(".hidden_quantity").val() > 0) {
        
        var variation_id  = jQuery(this).find('input[name="variation_id"]').val();
        var product_id    = jQuery(this).find('input[name="product_id"]').val();
        var quantity      = jQuery(this).find('input[name="quantity"]').val();
        var gift_wrap     = jQuery(this).find('input[name="gift_wrap"]').val();
        var variations    = jQuery(this).find('input[name="form_vartable_attribute_json"]').val();
        
        var vartable_ajax       = jQuery(this).closest('table.vartable').data('vartable_ajax');
        var vartable_globalcart = jQuery(this).closest('table.vartable').data('globalcart');
        var cartredirect        = jQuery(this).closest('table.vartable').data('cartredirect');
        
        var addvtdata = jQuery(this).serialize();
        var thisid = jQuery(this).attr("data-variation_id");
        var thisbutton = jQuery(this).find('button.add_to_cart');
        var thisbuttonid = thisbutton.attr('id').split('_');

        jQuery.ajax({
          type:"POST",
          url: ajaxurl,
          data: {
            "action" : "add_variation_to_cart",
            "product_id" : product_id,
            "variation_id" : variation_id,
            "quantity" : quantity,
            "gift_wrap" : gift_wrap
          },
          success:function(data){
            
            
            //conditionally perform fragment refresh
            if (vartable_ajax == 1) {
              $.ajax( $fragment_refresh );
            }
            
            jQuery("#added2cart_"+ thisbuttonid[1]).fadeIn(200, function() {
              jQuery("#added2cart_"+ thisbuttonid[1]).delay(1000).fadeOut(1000);
              
              // console.log('ajax: '+ vartable_ajax);
              // console.log('ajax: '+ vartable_globalcart);

              if ((vartable_ajax != 1 && (vartable_globalcart == 1 || vartable_globalcart == 2)) || cartredirect == 'yes') {
                if (gclicked == 0) {
                  if (cartredirect == 'yes') {
                    window.location.href = ""+carturl+"";
                  }
                  if (cartredirect == 'no') {
                    location.reload();
                  }
                }
              }
              
            });
          },
          error: function(data) {
            console.log(data);
          }
        });
        
      }
      
      return false;
      
    });
    
    if (jQuery(".globalcartbtn.submit").length > 0) {
      jQuery(".globalcartbtn.submit").on("click", function(event) {
              
        event.preventDefault();
        
        var clickthis = jQuery(this);
        var pid = clickthis.attr("id").split("_");
        
        var vartable            = jQuery('#tb_'+pid[1]);
        var random_id           = vartable.data('random');
        var vartable_ajax       = vartable.data('vartable_ajax');
        var cartredirect        = vartable.data('cartredirect');
        var sorting             = vartable.data('sort');
        var vartable_globalcart = vartable.data('globalcart');
        var preorder            = vartable.data('preorder');
        var preorder_direction  = vartable.data('preorder_direction');
        var position            = jQuery(this).data('position');
        
        
        jQuery(".vtspinner_"+ position +".vtspinner_"+ pid[1]).stop(true).fadeIn(100, function() {

          var count = jQuery("table#tb_"+ pid[1] +" tr").not(".descrow").length;
          // console.log("rows: "+ count);
          var trig = 0;
          jQuery.ajaxSetup({ async: false });
          jQuery("table#tb_"+ random_id +" tr").not(".descrow").each(function(index) {
            if(jQuery(this).find("input.globalcheck").length > 0 && jQuery(this).find("input.globalcheck").is(":checked") && jQuery(this).find("input.qty").val() > 0) {
              
              
              // var vpid = jQuery(this).find("input.globalcheck").attr("name").split("_");
              // jQuery("#add2cartbtn_"+vpid[1]).trigger("click");
              // jQuery("#vtvariation_"+vpid[1]).submit();
              // jQuery(this).find(".single_add_to_cart_button").trigger("click");
              jQuery(this).find("form.vtajaxform").submit();
              // console.log(jQuery("#add2cartbtn_"+vpid[1]).text());
              trig = 1;                    
            }
            count = count - 1;
            // console.log(count);
            if (vartable_ajax != 1 || cartredirect == 'yes') {
              if (count <= 0 && trig == 1) {
                if (cartredirect == 'yes') {
                  window.location.href = ""+carturl+"";
                }
                if (cartredirect == 'no') {
                  location.reload();
                }
              }
            }
            if (count <= 0 || trig == 0) { jQuery(".vtspinner_"+ pid[1]).stop(true, true).fadeOut(100); }
            if (trig == 1)  { jQuery(".added2cartglobal_"+ pid[1]).stop(true).fadeIn(200); jQuery(".added2cartglobal_"+ pid[1]).delay(3000).fadeOut(200); }
          });
        
        });

      });
    }
    
    
  }
  
  jQuery("input.input-text.qty").on("input", function() {
    
    var valmin  =jQuery(this).attr('min');
    var valmax  =jQuery(this).attr('max');
    
    
    if( typeof valmin === 'undefined' || valmin === null ){
      valmin = 0;
    }
    if( typeof valmax === 'undefined' || valmax === null ){
      valmax = -1;
    }
    
    if (parseInt(jQuery(this).val()) < valmin) {
      jQuery(this).val(valmin);
    }    
    if (parseInt(jQuery(this).val()) > valmax && valmax != -1) {
      jQuery(this).val(valmax);
    }
    
    jQuery(this).closest('tr').find(".cartcol input.hidden_quantity").val(jQuery(this).val());
    jQuery(this).trigger( "qty:change" );
  });
  
  if (jQuery('.giftcol').length > 0) {
    jQuery("input.var_gift_wrap").on("change", function() {
      if(jQuery(this).is(":checked")) {
        jQuery(this).closest('tr').find(".cartcol input.gift_wrap").val("yes");
      } else {
        jQuery(this).closest('tr').find(".cartcol input.gift_wrap").attr("value", "");
      }
    });
  }
  
  
  jQuery('input[name="var_quantity"]').on('qty:change', function() {
    update_global_sum(jQuery(this));
  });
  jQuery('input.globalcheck').on('change', function() {
    update_global_sum(jQuery(this));
  });
  
  function update_global_sum(object) {
    var random_id = object.closest('table.vartable').data('random');
    var numofchecked = 0;
    jQuery("table#tb_"+random_id+"").each(function(index) {
      
      jQuery(this).find('tr').each(function(row) {
        
        if (jQuery(this).find('.globalcheck').is(":checked")) {
          if (parseInt(jQuery(this).find('input[name="var_quantity"]').val()) > 0) {
            numofchecked = numofchecked + parseInt(jQuery(this).find('input[name="var_quantity"]').val());
          }
        }
        
      });
      
    });
    
    jQuery('#gc_'+ random_id +'_top span.vt_products_count, #gc_'+ random_id +'_bottom span.vt_products_count').text(' ('+ numofchecked +')');
    
  }
  
  jQuery(".vartable_selectall_check").on("change", function(event) {

    var said = jQuery(this).attr("id").split("_");

    if(this.checked) {
      jQuery("table#tb_"+ said[1] +" tr").each(function(index) {
        
        if(jQuery(this).find("input.globalcheck").length > 0) {
        
          jQuery(this).find("input.globalcheck").attr("checked", "checked");
        }          
        
      });
    } else {
      jQuery("table#tb_"+ said[1] +" tr").each(function(index) {
      
        if(jQuery(this).find("input.globalcheck").length > 0) {
          jQuery(this).find("input.globalcheck").removeAttr("checked");
        }
        
      });
    }
  });
  
  
});